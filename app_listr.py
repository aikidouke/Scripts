#!/usr/bin/python3

import os
import sqlite3


def scrape_term(linestring):
    terms = linestring.split('=')
    return_term = terms[1]
    return return_term


def get_app_data():
    app_main_list = []
    os.chdir('/usr/share/applications')
    work_dir = os.getcwd()
    applist = os.listdir()
    for app in applist:
        if os.path.isfile(os.path.join(work_dir, app)) == True:
            read_app = open(app, 'r')
            for line in read_app.readlines():
                if line.startswith('Name'):
                    app_name = scrape_term(line)
                elif line.startswith('Exec'):
                    app_exec = scrape_term(line)
                elif line.startswith('Categories'):
                    app_categs = scrape_term(line)
                    app_catg_list = []
                    try:
                        multi_cat = app_categs.split(';')
                        for list_item in multi_cat:
                            app_catg_list.append(list_item)
                    except:
                        continue
            app_listing = { 'app_name':app_name, 'app_exec':app_exec, 'app_catg_list':app_catg_list }
            read_app.close()
            app_main_list.append(app_listing)
    return app_main_list

def create_sql3(desktop_apps):
    app_db = sqlite3.connect('/home/aikidouke/data/app_db')
    app_db_crs = app_db.cursor()
    app_db_crs.execute('''
        CREATE TABLE IF NOT EXISTS apps(name TEXT PRIMARY KEY, exec TEXT, priCat TEXT, secCat TEXT, terCat TEXT)
    ''')
    app_db.commit()
    for app_item in desktop_apps:
        pri, sec, ter = '', '', ''
        appl_cats = (pri, sec, ter)                                                                                          
        sub = 0
        for sub_cat in app_item['app_catg_list']:
            try:
                appl_cats[sub] = sub_cat
                sub += 1
            except:
                continue
        try:
            app_db_crs.execute('''INSERT INTO apps(name, exec, priCat, secCat, terCat)
                VALUES(?,?,?,?,?)''', (app_item['app_name'], app_item['app_exec'], appl_cats[0], appl_cats[1], appl_cats[2]))
            print('app db updated')
        except sqlite3.IntegrityError:
            print('record for ', app_item['app_name'], ' already exits')
    app_db.commit()
    return app_db


desktop_apps = get_app_data()
create_sql3(desktop_apps)
while True:
    sc = input('Search for Cat: ')
    app_db = sqlite3.connect('/home/aikidouke/data/app_db')
    app_cur = app_db.cursor()
    try:
        app_answr = app_cur.execute('SELECT name FROM apps WHERE priCat=?', sc)
        app_nms = app_answr.fetchall()
        for app in app_nms:
            print(app)
    except:
        print('sad trombone')
        cat_answr = app_cur.execute('SELECT priCat FROM apps')
        app_cts = cat_answr.fetchall()
        for cat in app_cts:
            print(cat)

'''
    TODO: Figure out why I am not finding any app categories
'''

