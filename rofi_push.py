#!/usr/env/python3

import os
import subprocess
from tinydb import TinyDB, Query

#   This script should grab a list of apps I use from /usr/share/applications
#   shape them into something useful as input for rofi
#   and be executable
#   need to define the location of apps as above
#   the way rofi executes them
#   each time the program runs it should read the applications

### pseudocode ###

def get_apps():
    app_list = os.listdir('/usr/share/applications')
    return app_list

"""Returns a list of applications"""


def get_app_info(app):
    app_nfo = open(app, 'r', newline=None)
    app_name, app_cat, app_exe = '', '', ''
    for line in app_nfo.readlines():
        if line.startswith("Name="):
            app_name_line = line.split("=")
            app_name = app_name_line[1]
        elif line.startswith("Categories="):
            app_cat_line_strt = line.split("=")
            app_cat_line = app_cat_line_strt[1]
            try:
                app_catsplit = app_cat_line.split(";")
                app_cat = app_catsplit[0]
            except:
                app_cat = app_cat_line_strt[1]
        elif line.startswith("Exec="):
            eggsecln = line.split("=")
            app_exe = eggsecln[1]
    app_nfo.close()
    return(app_name, app_cat, app_exe)

"""Takes one .desktop file and returns the name, category, and executable"""


def create_app_dbase():
    os.chdir('/usr/share/applications')
    appdb = TinyDB('/home/greyman/.tmp/appdb.json')
    app_list = get_apps()
    for app in app_list:
        if os.path.isfile(app) == True:
            app_name, app_cat, app_exe = get_app_info(app)
            appdb.insert({'App': app_name, 'Cat': app_cat, 'Exe': app_exe})
    return(appdb)

"""Just pushes that app info into dbase"""

create_app_dbase()



            

                

